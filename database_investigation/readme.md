# Database investigation

What is in 16S Silva that we do not have in our custom DB and that justify the multi DB affi startegy?


## Steps: 
- Blast 16S silva over 16S-23S 
- Get the taxo info on sequence that are not correctly matching any 16S-23S sequence: Get number of species, genus, family .... 


## Blast 16S silva over 16S-23S 

Make blast db with the 16S-23S db 

```
ln -s /work/project/seqoccin/metaG/marker_identification/region_identification_workflow/frogs_db_formatting/16S-23S_bacteria_and_archaea_refseq_2022_02_07/refseq-all_16S-23S_2022-02-07_flagged/refseq-all_16S-23S_2022-02-07_flagged.fasta

makeblastdb -dbtype nucl -in refseq-all_16S-23S_2022-02-07_flagged.fasta  

```

Launch blast with 16S Silva 


/save/frogs/galaxy_databanks/SILVA/16S/silva_138.1_16S/silva_138.1_16S.fasta



## Parallel execution with a nextflow workflow


Blastn is very slow. Parallel execution usig nextflow in `nextflow_blast/`


see : https://www.nextflow.io/example3.html
wget https://raw.githubusercontent.com/nextflow-io/blast-example/master/main.nf 

```
cd nextflow_blast/
sbatch launcher.sh

python ../summarise_db1_vs_db2.py --blast_result silva23S_vs_custom16S23S.tsv --fasta_query silva_138.1_23S.fasta 

```

## Summarize stat comparison

Use the python script [summarise_db1_vs_db2.py](summarise_db1_vs_db2.py) to make a table to sumup the comparison. 

```bash
python ../summarise_db1_vs_db2.py --blast_result nextflow_blast/silva16S_vs_custom16S23S.tsv --fasta_query silva_138.1_16S.fasta -v
```


# Trying with 23S silva vs 16S23S custom db

Work in `23Ssilva_blast_to_custom16S23S`
Doing the same as with the 16S silva db. 
Use nextflow to paralellise the blastn. 

Get the summary table with
```
python ../summarise_db1_vs_db2.py --blast_result silva23S_vs_custom16S23S.tsv --fasta_query silva_138.1_23S.fasta -v -o silva23S_vs_custom16S23S_matching_ranks_count.tsv 

```

# Using another 1623S database:  ncbi_202006

This database come from this paper
Kinoshita, Yuta, et al. "Establishment and assessment of an amplicon sequencing method targeting the 16S-ITS-23S rRNA operon for analysis of the equine gut microbiome." Scientific reports 11.1 (2021): 1-12.  


The database has been downloaded here: /work/project/seqoccin/metaG/metabarcoding/16S23S_databases/ncbi_202006_DB/ncbi_202006.fasta.gz


## Construct blastdb
```
zcat ncbi_202006.fasta.gz | makeblastdb -dbtype nucl  -in - -out ncbi_202006.fasta -title ncbi_202006 
```

## launching nextflow blast 

Doing the same as with our custom db. 


## summup the comparison

```
python ../summarise_db1_vs_db2.py --blast_result nextflow_blast/silva16S_vs_ncbi_202006.tsv --fasta_query silva_138.1_16S.fasta -v -o silva16S_vs_ncbi_202006_matching_ranks_count.tsv
```