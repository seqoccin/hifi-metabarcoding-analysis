#!/usr/bin/env nextflow

/*
 *
 * Copyright (c) 2013-2018, Centre for Genomic Regulation (CRG).
 * Copyright (c) 2013-2018, Paolo Di Tommaso and the respective authors.
 *
 *   This file is part of 'Nextflow'.
 *
 *   Nextflow is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Nextflow is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Nextflow.  If not, see <http://www.gnu.org/licenses/>.
 */
 

/*
 * Defines the pipeline inputs parameters (giving a default value for each for them) 
 * Each of the following parameters can be specified as command line options
 */
params.query = "/save/frogs/galaxy_databanks/SILVA/16S/silva_138.1_16S/silva_138.1_16S.fasta"
params.db = "/work/project/seqoccin/metaG/metabarcoding/database_investigation/refseq-all_16S-23S_2022-02-07_flagged.fasta"
params.out = "silva16S_vs_custom16S23S.tsv"
params.chunkSize = 2260 

db_name = file(params.db).name
db_dir = file(params.db).parent


workflow {
    /*
     * Create a channel emitting the given query fasta file(s).
     * Split the file into chunks containing as many sequences as defined by the parameter 'chunkSize'.
     * Finally, assign the resulting channel to the variable 'ch_fasta'
     */
    Channel
        .fromPath(params.query)
        .splitFasta(by: params.chunkSize, file:true)
        .set { ch_fasta }

    /*
     * Execute a BLAST job for each chunk emitted by the 'ch_fasta' channel
     * and emit the resulting BLAST matches.
     */
    ch_hits = blast(ch_fasta, db_dir)

    /*
     * Collect all the sequences files into a single file
     * and print the resulting file contents when complete.
     */
    ch_hits
        .collectFile(name: params.out, storeDir:baseDir)
}


process blast {
    cpus 10
    memory '1 GB'
    executor 'slurm'

    input:
    path query
    path db

    output:
    path 'blast_results.tsv'

    """
    module load bioinfo/ncbi-blast-2.13.0+

    outfmt="qseqid qstart qend qlen sseqid sstart send slen length pident qcovhsp qcovs evalue bitscore"


    blastn -query $query -db $db/$db_name  -out blast_results.tsv -outfmt "6 \$outfmt" -evalue 0.00001 \
                                            -perc_identity 95 -max_hsps 1 -qcov_hsp_perc 95 -num_alignments 5 -num_threads ${task.cpus}

    """
}


