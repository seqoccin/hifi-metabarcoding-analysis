#!/bin/bash
#SBATCH -J blast_nf
#SBATCH -c 1
#SBATCH --mem=30GB
#SBATCH -t 03-00 #Acceptable time formats include "minutes", "minutes:seconds", "hours:minutes:seconds", "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds".
#SBATCH --output=slurm-%x.%j.out
#SBATCH -p workq



module load bioinfo/nfcore-Nextflow-v22.12.0-edge

nextflow run main.nf -with-report -with-trace  