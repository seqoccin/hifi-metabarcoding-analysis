# getting the fastq data 
mkdir fastq
cp -P  /work2/project/seqoccin/metaG/metabarcoding/polymerase_test/fastq/*Pbr322* fastq/

# fastqc

sbatch fastqc.sbatch


# download ref sequence of the plasmide

## from ncbi web site   
ncbi page: https://www.ncbi.nlm.nih.gov/nuccore/J01749.1?report=fasta

Download manually the sequence in plasmide_Pbr322_ncbi.fasta 
## from NEB web site 

https://international.neb.com/-/media/nebus/page-images/tools-and-resources/interactive-tools/dna-sequences-and-maps/text-documents/pbr322fsa.txt?rev=b1ef8762bbe9431886127f019c09df5b&hash=355F5D1EE26BAC8271AA68193BF15F36


download manually in plasmide_Pbr322_neb.fasta


## seq comparison
1 nt difference between the 2 seq 

https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=emboss_needle-I20210601-152410-0083-31480283-p2m

```
pBR322          1101 CGCTCGCGGCTCTTACCAGCCTAACTTCGATCATTGGACCGCTGATCGTC   1150
                     |||||||||||||||||||||||||||||||||.||||||||||||||||
J01749.1        1101 CGCTCGCGGCTCTTACCAGCCTAACTTCGATCACTGGACCGCTGATCGTC   1150
```

# cleaning the reads 
Pacbio adaptateur have not been removed by pacbio postprocess.



Primer + adaptateur
fwd :GCAGTCGAACATGTAGCTGACTCAGGTCACAGCTTTAATGCGGTAGTTTATC

rev : TGGATCACTTGTGCAAGCATCACATCGTAGATCGATGATAAGCTGTCAAAC


adaptateur Pacbio:

fwd GCAGTCGAACATGTAGCTGACTCAGGTCAC

rev : TGGATCACTTGTGCAAGCATCACATCGTAG





## using frogs preprocess tool

let's remove them with the primer sequences using preprocess of frogs. 

simple test on the first 200 reads 

```

zcat ../fastq/Accustart-Pbr322-PCR1-1.fastq.gz | head -n800 > h800_Accustart-Pbr322-PCR1-1.fastq

five_prime_primer=GCAGTCGAACATGTAGCTGACTCAGGTCACAGCTTTAATGCGGTAGTTTATC
#three_prim_primer=TGGATCACTTGTGCAAGCATCACATCGTAGATCGATGATAAGCTGTCAAAC  need to be reverse complement to work with frogs
three_prim_primer=GTTTGACAGCTTATCATCGATCTACGATGTGATGCTTGCACAAGTGATCCA

preprocess.py pacbio-hifi --min-amplicon-size 4000  --max-amplicon-size 4500                        --five-prim-primer $five_prime_primer --three-prim-primer $three_prim_primer -p 2                           --input-reads h800_Accustart-Pbr322-PCR1-1.fastq 
```


preprocess on all samples 


sort preprocess_counts.tsv file to see the more abundant sequence

sort -k2 -rn preprocess_counts.tsv | head

### launch frogs on the plasmides 

in dir frogs_pipeline
run frogs until otu filters (no affiliation.. )




# Aligning the reads to the plasmid 


In mapping_reads_to_plasmide/test

```
module load bioinfo/samtools-1.11
module load bioinfo/minimap2-2.11

reads="/work2/project/seqoccin/metaG/metabarcoding/polymerase_test/plasmide_Pbr322_analysis/fastq/Accustart-Pbr322-PCR1-1.fastq.gz"
ref='/work2/project/seqoccin/metaG/metabarcoding/polymerase_test/plasmide_Pbr322_analysis/plasmide_ref_sequence/plasmide_Pbr322_ncbi.fasta'

read_name=`basename ${reads%.fastq.gz}`
ref_name=`basename ${ref%.fasta}`

output_basename=${read_name}_to_${ref_name}
echo $output_basename

minimap2 -ax asm20 $ref $reads | samtools sort -o ${output_basename}.bam


samtools index ${read_name}_to_${ref_name}.bam
samtools flagstat ${read_name}_to_${ref_name}.bam > ${read_name}_to_${ref_name}.flagstat

samtools coverage  ${read_name}_to_${ref_name}.bam > ${read_name}_to_${ref_name}.coverage
```

In IGV, we can clearly see that a position 1134 a T is present and not a C. --> the correct ref is the one from neb web site ! 


## map frogs preprocessed reads to plasmide sequence

in frogs_preprocess/


run script align_preprocess_reads_to_plasmide_seq.sbatch 


## map frogs preprocessed reads to expected amplicon of the plasmide

instead f takinf the whole plasmide sequence it will be easier to map the read only on the expected amplicon sequence (subsequence of the plasmide). 
It will much easier to compute error rate at each position.

run script align_preprocess_reads_to_expected_amplicon_seq.sbatch



# Analyse amplicon error

type of analysis already performed here : /work2/project/seqoccin/metaG/metabarcoding/16S-23S_27_10_2020/ADNmockbact/frogs_results/preprocess_affiliation/map_amplicon_to_genomes 

It has also been made here along dada2 analysis of ccs data : https://benjjneb.github.io/LRASManuscript/LRASms_Zymo.html 

Filter bam to have a smaller dataset to work with

```
module load bioinfo/samtools-1.11
conda activate jupy
cp ~/metaG/axis-3-whole-metagenome/A3P2_samples/scripts/filter_samfile_by_read_list.py .
# modification of filter_samfile_by_read_list.py to parse read name by removing the size info to be able to identify them.. 

samtools view preprocess_reads_to_expected_seq.bam | python filter_samfile_by_read_list.py  -r best_2k_preprocess_reads.list -v > best_2k_preprocess_reads_to_expected_seq.sam

```
# Get error rate from shotgun 

In dir shotgun_analysis 

cp -P /work2/project/seqoccin/metaG/metabarcoding/polymerase_test/fastq/PBR322-proto-shotgun-ligation.fastq.gz . 


module load bioinfo/FastQC_v0.11.7
module load bioinfo/MultiQC-v1.7

mkdir -p fastqc
fastqc PBR322-proto-shotgun-ligation.fastq.gz -o fastqc/ 
multiqc fastqc/


map ll












