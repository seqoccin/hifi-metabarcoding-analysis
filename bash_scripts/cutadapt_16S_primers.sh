#!/bin/bash
#SBATCH -J cutadapt
#SBATCH -c 20
#SBATCH --mem=5GB
#SBATCH -t 03-00 #Acceptable time formats include "minutes", "minutes:seconds", "hours:minutes:seconds", "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds".
#SBATCH --output=slurm-%x.%j.out
#SBATCH -p workq



# 16S primers

five_prime_primer=AGRGTTYGATYMTGGCTCAG
three_prim_primer=AAGTCGTAACAAGGTARCY

outdir=fastq_no_primers

mkdir -p $outdir/tmp

t=20

for fastq_file in ../fastq/*fastq.gz; do 

    echo "======================================="

    fastq_file_name=`basename $fastq_file`
    outfile_5prim="$outdir/tmp/${fastq_file_name%.fastq.gz}_cutadapt_no5prim.fastq.gz"
    outfile_final="$outdir/${fastq_file_name%.fastq.gz}.fastq.gz"

    # REMOVE PACBIO Primers 


    primer_length=${#five_prime_primer}
    overlap="$((primer_length-1))"
    cutadapt -g $five_prime_primer --error-rate 0.1 --discard-untrimmed --match-read-wildcards --revcomp -j $t --overlap $overlap -o $outfile_5prim $fastq_file > ${outfile_5prim%.fastq.gz}.log ; head -n40 ${outfile_5prim%.fastq.gz}.log

    primer_length=${#three_prim_primer}
    overlap="$((primer_length-1))"
    cutadapt -a $three_prim_primer --error-rate 0.1 --discard-untrimmed --match-read-wildcards -j $t --overlap $overlap -o $outfile_final $outfile_5prim > ${outfile_final%.fastq.gz}.log;  head -n40  ${outfile_final%.fastq.gz}.log


done

