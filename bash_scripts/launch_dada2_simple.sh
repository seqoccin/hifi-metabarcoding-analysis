#!/bin/bash
#SBATCH -J dada2
#SBATCH -c 20
#SBATCH --mem=20GB
#SBATCH -t 03-00 #Acceptable time formats include "minutes", "minutes:seconds", "hours:minutes:seconds", "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds".
#SBATCH --output=slurm-%x.%j.out
#SBATCH -p unlimitq



module load system/pandoc-2.1.3

module load load system/Miniconda3-4.7.10
source /usr/local/bioinfo/src/Miniconda/Miniconda3-4.7.10/etc/profile.d/conda.sh
conda activate dada2

# To be working the Rmd file need to be copied. it can't be a symbolic link ! 

# cp ../scripts/dada2_analysis_simple.Rmd . 

Rscript -e 'rmarkdown::render("dada2_analysis_simple.Rmd")'

