#!/bin/bash
set -e

jobid_array=()
seff_array=()
## FUNCTIONS ## 
launch_command() {

    local cmd=$1
    local nb_cpu=$2
    local mem=$3
    local last_job_id=$4
    
    local cmd_name=`echo $cmd |cut -d' ' -f1 | cut -d'.' -f1`
    
    echo
    echo "## $cmd_name ##"
    
    if [ -z "$last_job_id" ] 
    then
          echo "No job dependency"
          local dependency=""
    else
          echo "depends on job $last_job_id"
          local dependency="--dependency=afterok:$last_job_id" 
    fi
                     
    sbatch_cmd="sbatch $dependency -J $cmd_name -o slurm-%x.%j.out --mem=$mem --cpus-per-task=$nb_cpu --wrap=\"$env_setting;$cmd\""
    

    echo
    echo $cmd
    echo
    echo $sbatch_cmd
    echo

    job_id=$(eval $sbatch_cmd | cut -d' ' -f4)
    #job_id=DRYRUN_ID_$cmd_name
    
    

    echo $cmd_name job $job_id depends on $last_job_id
    
    echo $job_id
    
    jobid_array+=("$job_id")
    sep='====================='
    seff_array+=("echo;echo $sep;echo COMMANDE: $cmd_name;echo;seff $job_id;")
    
    
    
}

cluster_stat () {
    
    last_cmdname=$1
    biom=$2
    last_job_id=$3

    nb_cpu=2
    mem=5GB
    cmd="clusters_stat.py -i $biom \
                        -o clusters_stat_${last_cmdname}.html \
                        --log-file clusters_stat_${last_cmdname}.log"


    launch_command "$cmd" $nb_cpu $mem $last_job_id

}


min_sample_presence=$1
if [ -z "$min_sample_presence" ] 
then    
       min_sample_presence=2
       echo "no parameter found so min_sample_presence=$min_sample_presence"
      
else
     echo "min_sample_presence=$min_sample_presence"
fi
   

fastq_files="fastq/16Sfull_zymomock.tar.gz"

# 16S primers
pacbio_adapt_fwd='GCAGTCGAACATGTAGCTGACTCAGGTCAC'
pacbio_adapt_rev_rc='CTACGATGTGATGCTTGCACAAGTGATCCA'

five_prime_primer=${pacbio_adapt_fwd}AGAGTTTGATCMTGGCTCAG
three_prim_primer=AAGTCRTAACAAGGTARCCGTA${pacbio_adapt_rev_rc}



# found db there:/save/frogs/galaxy_databanks/
# with help of http://genoweb.toulouse.inra.fr/frogs_databanks/assignation/readme.txt


# 16S-23S
#database='/work2/project/seqoccin/metaG/marker_identification/region_identification_workflow/frogs_db_formatting/rep_ref_and_10_assemblies_per_sp_refseq_2020-06-09/refseq-rr10_16S-23S_2020-06-09/refseq-rr10_16S-23S_2020-06-09.fasta' 
#taxonomic_ranks='Domain Phylum Class Order Family Genus Species Strain'

# 16S 
database="/save/frogs/galaxy_databanks/SILVA/16S/silva_138.1_16S/silva_138.1_16S.fasta"

taxonomic_ranks='Domain Phylum Class Order Family Genus Species'

# environnement setting
frogs_path=/home/jmainguy/work/FROGS/

env_variable="export PYTHONPATH=$frogs_path/lib:\$PYTHONPATH;\
              export PATH=$frogs_path/libexec/:\$PATH;\
              export PATH=$frogs_path/app/:\$PATH"


conda_env="module load system/Anaconda3-5.2.0 ; \
           source /usr/local/bioinfo/src/Anaconda/Anaconda3-5.2.0/etc/profile.d/conda.sh; \
           conda activate frogs"

env_setting="$env_variable;$conda_env"
echo $env_setting




## COMMANDES & EXECUTION ##

## Preprocess ##

nb_cpu_preprocess=2
mem_preprocess=20GB


preprocess_cmd="preprocess.py pacbio-hifi --min-amplicon-size 1000  --max-amplicon-size 2000 \
                         --five-prim-primer $five_prime_primer --three-prim-primer $three_prim_primer \
                          --input-archive $fastq_files -p $nb_cpu_preprocess"


launch_command "$preprocess_cmd" $nb_cpu_preprocess $mem_preprocess
preprocess_job_id=$job_id


## Clustering ## 

nb_cpu_clustering=10
mem_clustering=20000



clustering_cmd="clustering.py --fastidious --distance 1 --input-fasta preprocess.fasta --input-count preprocess_counts.tsv \
               --nb-cpus $nb_cpu_clustering --output-biom clustering.biom \
              --output-fasta clustering.fasta --log-file clustering.log"
              

launch_command "$clustering_cmd" $nb_cpu_clustering $mem_clustering $preprocess_job_id

clustering_job_id=$job_id

cluster_stat  "clustering" clustering.biom $clustering_job_id

## Remove chimera

nb_cpu_rm_chimera=12
mem_remove_chimera=36GB



remove_chimera_cmd="remove_chimera.py --input-fasta clustering.fasta --input-biom clustering.biom --nb-cpus $nb_cpu_rm_chimera\
                  --out-abundance remove_chimera.biom --non-chimera remove_chimera.fasta \
                  --log-file remove_chimera.log --summary remove_chimera.html"


launch_command "$remove_chimera_cmd" $nb_cpu_rm_chimera $mem_remove_chimera $clustering_job_id
remove_chimera_job_id=$job_id 

cluster_stat  "remove_chimera" remove_chimera.biom $remove_chimera_job_id


# Otu filter


nb_cpu_otu_filters=1
mem_otu_filters=30GB



filters_cmd="otu_filters.py --min-abundance 0.00005 --min-sample-presence $min_sample_presence --nb-cpus $nb_cpu_otu_filters --input-fasta remove_chimera.fasta --input-biom remove_chimera.biom \
               --output-biom filters.biom --output-fasta filters.fasta --log-file filters.log --summary filters.html"



launch_command "$filters_cmd" $nb_cpu_otu_filters $mem_otu_filters $remove_chimera_job_id
filters_job_id=$job_id 

cluster_stat "otu-filters" filters.biom $filters_job_id


# affiliation OTU

nb_cpu_affiliation_OTU=32
mem_affiliation=100GB


affiliation_cmd="affiliation_OTU.py --nb-cpus $nb_cpu_affiliation_OTU --reference $database --input-biom filters.biom \
                   --input-fasta filters.fasta --output-biom affiliation_abundance.biom \
                   --summary affiliation.html --log-file affiliation.log --taxonomy-ranks $taxonomic_ranks"


launch_command "$affiliation_cmd" $nb_cpu_affiliation_OTU $mem_affiliation $filters_job_id
affiliation_job_id=$job_id 


# affiliation stat

affiliation_stat_cmd="affiliations_stat.py -i affiliation_abundance.biom  \
                                           --tax-consensus-tag 'blast_taxonomy' \
                                          --identity-tag 'perc_identity' \
                                          --coverage-tag 'perc_query_coverage' \
                                          --multiple-tag 'blast_affiliations' \
                                          --rarefaction-ranks Family Genus Species \
                                          --taxonomic-ranks $taxonomic_ranks"
                                          
launch_command "$affiliation_stat_cmd" '1' "5GB" $affiliation_job_id


# Biom to tsv
biom2tsv_cmd="biom_to_tsv.py --input-biom affiliation_abundance.biom \
                    --input-fasta filters.fasta --output-tsv affiliation_abundance.tsv \
                     --output-multi-affi multiaff.tsv --log-file biom_to_tsv_affi.log"


launch_command "$biom2tsv_cmd" '1' "5GB" $affiliation_job_id



echo Need to cancel?
echo scancel "${jobid_array[@]}"
echo "${seff_array[@]}" 


