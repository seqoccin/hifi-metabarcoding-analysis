#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = "Mainguy Jean - Plateforme bioinformatique Toulouse"
__copyright__ = "Copyright (C) 2020 INRAE"
__license__ = "GNU General Public License"
__version__ = "0.1"
__email__ = "support.bioinfo.genotoul@inra.fr"
__status__ = "dev"


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import pandas as pd
from ete3 import Tree
from itertools import cycle

import os


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(
        description="...", formatter_class=ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("--affiliation_table", required=True, help="...")

    parser.add_argument("-o", "--outname", default="affiliation_tree", help="...")

    parser.add_argument(
        "--samples",
        nargs="+",
        type=str,
        default=None,
        help="list of samples to take affiliation from. Default take all affiliations into account",
    )

    parser.add_argument(
        "-v", "--verbose", help="increase output verbosity", action="store_true"
    )

    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Mode verbose ON")

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    tax_table = args.affiliation_table
    samples = args.samples
    valid_col = 'valid_affiliation'

    samples_concat_str = "-".join(samples)

    outdir = args.outname
    os.makedirs(outdir, exist_ok=True)

    logging.info(f"Output are stored in outdir {outdir}")

    df = pd.read_csv(tax_table, sep="\t")

    assert all(( type(v) is bool  for v in set(df[valid_col])) ), f'Not all value of {valid_col} column is a boolean. So we cannot filter out weak affiliations for the plot.'
    #keep only row with a valid affiliation. 
    df = df.loc[df[valid_col]] 

    # Filter samples
    # remove row that have no count in the given samples colonnes

    assert all(
        (sample in df.columns for sample in samples)
    ), f"Given samples {samples} are not found in given table columns {df.columns}"

    if samples:
        samples_filt = df[samples].sum(axis=1) != 0
        df = df.loc[samples_filt]

    # Build taxonomy tree with ete3

    parent_child_set = set()
    ranks = [
        "Domain",
        "Phylum",
        "Class",
        "Order",
        "Family",
        "Genus",
        "Species",
    ]

    for taxo in set(df["blast_taxonomy_cleaned"]):
        lineage = [t for t in taxo.split(";") if t != "Multi-affiliation"]
        # add rank to lineage to avoid confusion
        lineage_with_rank = [f"{t}_{r}" for t, r in zip(lineage, ranks)]

        parent_child_set |= {
            (child, parent, 1)
            for child, parent in zip(lineage_with_rank[:-1], lineage_with_rank[1:])
        }

    tree = Tree.from_parent_child_table(list(parent_child_set))

    tree_out_file = os.path.join(outdir, f"{outdir}_s{samples_concat_str}_tree.nw") 
    logging.info(f"write affiliation tree in newick: {tree_out_file}")

    tree.write(format=1, outfile=tree_out_file)

    # import seaborn as sns
    # tab20 = list(sns.color_palette("tab20"))
    # tab20 = [f"rgb({r*250:.0f}, {g*250:.0f}, {b*250:.0f})" for r,g,b in tab20]
    colors = [
        "rgb(30, 117, 176)",
        "rgb(171, 195, 227)",
        "rgb(250, 125, 14)",
        "rgb(250, 183, 118)",
        "rgb(43, 157, 43)",
        "rgb(149, 219, 135)",
        "rgb(210, 38, 39)",
        "rgb(250, 149, 147)",
        "rgb(145, 101, 185)",
        "rgb(193, 173, 209)",
        "rgb(137, 84, 74)",
        "rgb(192, 153, 145)",
        "rgb(223, 117, 190)",
        "rgb(242, 178, 206)",
        "rgb(125, 125, 125)",
        "rgb(195, 195, 195)",
        "rgb(184, 185, 33)",
        "rgb(215, 215, 138)",
        "rgb(23, 186, 203)",
        "rgb(155, 214, 225)",
    ]
    colors_iter = cycle(colors)

    taxonomies = set(df["blast_taxonomy_cleaned"])
    phylums = {taxo.split(';')[1] for taxo in taxonomies} # index 1 for phylum}

    # write itol TREE_COLORS style
    
    phylum_colors_file = os.path.join(outdir, f"{outdir}_s{samples_concat_str}_colors_styles_phylum.txt")
    
    logging.info(f"write phylum itol TREE_COLORS metadata: {phylum_colors_file}")

    with open(phylum_colors_file, "w") as fl:
        fl.write("TREE_COLORS\nSEPARATOR TAB\n\nDATA\n")
        fl.write("#NODE_ID TYPE COLOR LABEL\n")
        for phylum, color in zip(phylums, colors_iter):
            # logging.debug(f"{phylum}, {color}")
            color_info = "\t".join([f"{phylum}_Phylum", "range", color, phylum])
            fl.write(color_info + "\n")


if __name__ == "__main__":
    main()
