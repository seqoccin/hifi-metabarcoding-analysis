#!/usr/bin/env python3

"""
Description

:Example:
python template.py -v
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme biodebugrmatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.biodebug.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import logging
import sys
import frogs_analysis_fct 
import pandas as pd

def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('--table_16S23Sdb', required=True, help='Affiliation abundance table with multiaffi debug of the 16S23S db affiliation, output of the script add_multiaffi_to_abd_table.py ')

    parser.add_argument('--table_16Sdb', required=True, help='Affiliation abundance table with multiaffi debug of the 16S db affiliation, output of the script add_multiaffi_to_abd_table.py ')
    parser.add_argument('--table_23Sdb', required=True, help='Affiliation abundance table with multiaffi debug of the 23S db affiliation, output of the script add_multiaffi_to_abd_table.py ')
    
    parser.add_argument('--min_identity', default=98, type=float, help='Minimal identity to consider an affilition as valid.')
    parser.add_argument('--min_qcov_23S', default=40, type=float, help='Minimal coverage of a 23S16S  query sequence on 23S subject sequence to consider the affilition valid.')
    parser.add_argument('--min_qcov_16S', default=30, type=float, help='Minimal coverage of a 23S16S  query sequence on 16S subject sequence to consider the affilition valid.')

    parser.add_argument('--min_qcov_16S23S', default=98, type=float, help='Minimal coverage of a 16S-23S query sequence on 16S23S subject sequence to consider the affilition valid.')


    parser.add_argument('-o', '--output', default='affiliation_multiple_db_merged.tsv', help='output table name')


    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--debug", help="increase a lot output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args

def add_db_to_specific_db_columns(df, db_specific_cols, db_name):
    df = df[db_specific_cols]

    new_col_names = {c:f'{db_name}_{c}' for c in df.columns}
    df = df.rename(columns=new_col_names)
    return df
# iterate through each cluster and unify affiliation 
# When 16S 23S affiliation is weak, the 16S and 23S is used. 
from itertools import takewhile

def all_equal(items) -> bool:
    '''
    A helper function to test if 
    all items in the given iterable 
    are identical. 

    Arguments:
    item -> the given iterable to be used

    eg.
    >>> all_same([1, 1, 1])
    True
    >>> all_same([1, 1, 2])
    False
    >>> all_same((1, 1, 1))
    True
    >> all_same((1, 1, 2))
    False
    >>> all_same("111")
    True
    >>> all_same("112")
    False
    '''
    return all(item == items[0] for item in items)

def common_prefix(its):
    return [items[0] for items in takewhile(all_equal, zip(*its))]

def manage_strain_in_taxo(taxonomy):
    logging.debug(taxonomy)
    assert len(taxonomy) == 7
    if taxonomy[-1].count(' ') > 1 and 'metagenome' not in taxonomy[-1]:
        logging.debug('taxonomy species look like strain')
        sp = ' '.join(taxonomy[-1].split(' ')[:2])
        taxonomy = taxonomy[:-1] + [sp, taxonomy[-1]]
    logging.debug(taxonomy)
    return taxonomy

def get_common_taxonomy(taxonomies):
    for t in taxonomies:
        logging.debug(t)
        
    taxonomies_list = (manage_strain_in_taxo(t.split(";")) for t in taxonomies)
    taxonomy =  common_prefix(taxonomies_list)
    
    missing_taxon_count = 7 - len(taxonomy)
    
    return ';'.join(taxonomy + ['Multi-affiliation']*missing_taxon_count)

def get_consensus_taxo(affi1, affi2, rank_count):
    for i in range(rank_count, 0, -1):
        affi1_i = {';'.join(t.split(';')[:i]) for t in affi1}
        affi2_i = {';'.join(t.split(';')[:i]) for t in affi2}
        shared_affi = affi1_i & affi2_i
        if len(shared_affi) == 0:
            continue

        missing_taxon_count = rank_count - i

        affi_found_in_both = { ';'.join(taxonomy.split(";") + ['Multi-affiliation']*missing_taxon_count) for taxonomy in shared_affi}

        if len(affi_found_in_both) == 1:
            return affi_found_in_both.pop()
        else:
            return get_common_taxonomy(affi_found_in_both)
        
    
    return  ';'.join(['Multi-affiliation'] * rank_count)
        

def get_consensus_affi(affi1, affi2, ranks):
    ranks_count = len(ranks)
    affi_origin = affi1['affiliation_origin'] + ' & '+ affi2['affiliation_origin']
    
    affiliation = {k:None for k in affi1}
    affiliation['affiliation_origin'] = affi_origin
    
    if affi1['blast_taxonomy'] == affi2['blast_taxonomy']:
        affiliation = {k:v for k, v in affi1.items()}
    
    taxonomies = set()
    
    
    if pd.isna(affi1['mutliaffiliation']) :
        
        taxonomies.add(affi1['blast_taxonomy'])
        taxonomies1 = {affi1['blast_taxonomy']}
    else:
        taxonomies |= set(affi1['mutliaffiliation'].split('|'))
        taxonomies1 = set(affi1['mutliaffiliation'].split('|'))
    
    if pd.isna(affi2['mutliaffiliation']) :
    
        taxonomies.add(affi2['blast_taxonomy'])
        taxonomies2 = {affi2['blast_taxonomy']}
    else:
        taxonomies |= set(affi2['mutliaffiliation'].split('|'))
        taxonomies2 = set(affi2['mutliaffiliation'].split('|'))
    
    taxonomy = get_consensus_taxo(taxonomies1, taxonomies2, ranks_count)
    #taxonomy = get_common_taxonomy(taxonomies)

    logging.debug('16S affi')
    [logging.debug(t) for t in taxonomies1]
    
    #logging.debug(affi2)
    logging.debug('23S affi')
    [logging.debug(t) for t in taxonomies2]
    logging.debug("CONSENSUS")
    logging.debug(taxonomy)
    
    affiliation['blast_taxonomy'] = taxonomy
    affiliation['mutliaffiliation'] = '|'.join(taxonomies)
    affiliation['blast_perc_identity'] = min((affi2['blast_perc_identity'], affi1['blast_perc_identity']))
    affiliation['blast_perc_query_coverage'] = min((affi2['blast_perc_query_coverage'], affi1['blast_perc_query_coverage']))
    
    
    affiliation['taxon_affi'] = frogs_analysis_fct.get_taxon_affi(taxonomy, ranks)
    affiliation['rank_affi'] = frogs_analysis_fct.get_rank_affi(taxonomy, ranks)
    
    affiliation['affiliation_origin'] =  affi_origin
    return affiliation

def get_affiliation_from_db(row, db_name):
    db_row = {k.replace(db_name+'_', ''):v for k, v in row.items() if k.startswith(db_name)}
    db_row['affiliation_origin'] = db_name
    return db_row

    
def unify_affiliation(row, ranks, min_id = 98, min_cov_16S23S=99,min_cov_16S = 30, min_cov_23S = 40  ):
    is_valid = True


    is_16S23S_valid = row["16S23Sdb_blast_perc_identity"] >= min_id and row["16S23Sdb_blast_perc_query_coverage"] >= min_cov_16S23S
    is_16Ssilva_valid = row["16Sdb_blast_perc_identity"] >= min_id and row["16Sdb_blast_perc_query_coverage"] >= min_cov_16S
    is_23Ssilva_valid = row["23Sdb_blast_perc_identity"] >= min_id and row["23Sdb_blast_perc_query_coverage"] >= min_cov_23S
    
    logging.debug(f'is_16S23S_valid {is_16S23S_valid}')
    logging.debug(f'is_16Ssilva_valid {is_16Ssilva_valid}')
    logging.debug(f'is_23Ssilva_valid {is_23Ssilva_valid}') 
    
    if is_16S23S_valid:
        affiliation = get_affiliation_from_db(row, '16S23Sdb')
        
    elif is_16Ssilva_valid and is_23Ssilva_valid:
        
        affiliation16S = get_affiliation_from_db(row, '16Sdb')
        
        affiliation23S = get_affiliation_from_db(row, '23Sdb')
        
        affiliation = get_consensus_affi(affiliation16S, affiliation23S, ranks)

    
    elif is_16Ssilva_valid and not is_23Ssilva_valid:
        affiliation = get_affiliation_from_db(row, '16Sdb')
        
    elif is_23Ssilva_valid and not is_16Ssilva_valid:
        affiliation = get_affiliation_from_db(row, '23Sdb')
        
    else:
        # affi is bad every where let's return the 16S23S one.
        affiliation = get_affiliation_from_db(row, '16S23Sdb')
        affiliation['affiliation_origin'] = "weak affiliation"
        is_valid = False


    affiliation['valid_affiliation'] = is_valid
    return affiliation




def main():

    args = parse_arguments()

    if args.debug:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.debug('Mode debug ON')
    elif args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")


    # specific columns that change for each db used for affiliation. 
    db_specific_cols = ["blast_taxonomy",
                "valid_affiliation",
                'blast_evalue',
                "blast_subject",
                "blast_perc_identity",
                "blast_perc_query_coverage",
                "blast_aln_length",
                "taxon_affi",
                "rank_affi",
                "mutliaffiliation"]
    ranks = 'Domain Phylum Class Order Family Genus Species'.split(' ')


    affi_16S23Sdb_table =  args.table_16S23Sdb # "./abundance_and_multiaffi_16S23S_custom.tsv"
    affi_16Sdb_table =  args.table_16Sdb # "./abundance_and_multiaffi_16Ssilva.tsv"
    affi_23Sdb_table = args.table_23Sdb # "./abundance_and_multiaffi_23Ssilva.tsv"

    min_id =  args.min_identity # 98
    min_cov_16S23S =  args.min_qcov_16S23S # 99
    min_cov_16S = args.min_qcov_16S # 30
    min_cov_23S = args.min_qcov_23S # 40

    logging.info("Parse affiliation tables")
    
    df_affi_16S23Sdb = pd.read_csv(affi_16S23Sdb_table, sep='\t').set_index('seed_id', drop=False)

    df_affi_23Sdb = pd.read_csv(affi_23Sdb_table, sep='\t').set_index('seed_id', drop=False)
    
    df_affi_16Sdb = pd.read_csv(affi_16Sdb_table, sep='\t').set_index('seed_id', drop=False)
    
    
    # Create a table with shared column.. so columns that have the same value for all db
    common_cols = set(df_affi_16Sdb.columns) - set(db_specific_cols)
    df_common = df_affi_16Sdb[[c for c in df_affi_16Sdb.columns if c in common_cols] ]
    df_common = df_common.set_index('seed_id', drop=False)

    # add db name to the specific columns
    df_affi_16Sdb = add_db_to_specific_db_columns(df_affi_16Sdb, db_specific_cols, "16Sdb")
    df_affi_23Sdb = add_db_to_specific_db_columns(df_affi_23Sdb, db_specific_cols, "23Sdb")
    df_affi_16S23Sdb = add_db_to_specific_db_columns(df_affi_16S23Sdb, db_specific_cols, "16S23Sdb")


    # merge affiliation tables
    logging.info("concat affiliation tables")
    df = pd.concat([df_common, 
                df_affi_16Sdb,
                df_affi_23Sdb,
                df_affi_16S23Sdb] , axis=1)

    # Some clusters in 16S23S db could have been removed. Let's check that and log some warning if this is the case
    df_rm_suspicious = df.loc[df['16S23Sdb_blast_taxonomy'].isna()]
    if len(df_rm_suspicious):
        logging.warning(f"{len(df_rm_suspicious)} clusters are not found in 16S-23S affiliation table. These clusters have probably been removed by affilaition filters because they had suspicious affiliation in the 16S3S db.")
        sum_seq_in_rm_clusters = df_rm_suspicious['observation_sum'].sum()
        abd_of_rm_clusters = 100 * df_rm_suspicious['observation_sum'].sum()/df['observation_sum'].sum()
        logging.warning(f"They represent a total of {sum_seq_in_rm_clusters} sequences and a relative abundance of {abd_of_rm_clusters:.3f}%")
        logging.warning('These clusters are ignored.')
        # remove cluster that does not appear in 16S23S because it has been removed  by affiliation filtering due to suspicious affiliation
        df = df.loc[~df['16S23Sdb_blast_taxonomy'].isna()]

    logging.info("unify/merge the affiliations")
    rows = []
    for _, row in df.iterrows():
        logging.debug('=')
        unified_affi = unify_affiliation(row, ranks, min_id, min_cov_16S23S, min_cov_16S, min_cov_23S)
        
        otu_debug = {}
        otu_debug.update(dict(row))
        otu_debug.update(dict(unified_affi))

        # clean taxonomy --> remove uninformative taxa : taxon with "unknown" or "metagenome" in their name
        otu_debug['blast_taxonomy_cleaned'] = frogs_analysis_fct.clean_taxonomy(unified_affi['blast_taxonomy'], ranks)

        rows.append(otu_debug)


    df_merged = pd.DataFrame(rows)
    df_merged['abundance'] = 100 *  df_merged['observation_sum']/df_merged['observation_sum'].sum()


    logging.info(f'Writting merged table in {args.output}')
    df_merged.to_csv(args.output, sep="\t", index=False)


if __name__ == '__main__':
    main()
